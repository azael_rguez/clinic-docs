from django.db import models
from django.utils import timezone


class Paciente(models.Model):
    id_unico = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    SEX_CHOICES = (
        ('H', 'Hombre'),
        ('M', 'Mujer')
        )
    sexo = models.CharField(max_length=10, choices=SEX_CHOICES, default='H')
    edad = models.IntegerField(default=0)
    peso = models.FloatField(default=0)
    estatura = models.FloatField(default=0)
    SINO_CHOICES = (
        ('S', 'Si'),
        ('N', 'No')
        )
    diabetes = models.CharField(max_length=10, choices=SINO_CHOICES, default='N')
    hipertencion = models.CharField(max_length=10, choices=SINO_CHOICES, default='N')
    diagnostico = models.TextField()
    recomendacion = models.TextField()
    creado_en = models.DateTimeField(
            default=timezone.now)
    actualizado_en = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.actualizado_en = timezone.now()
        self.save()
