from django.contrib import admin
from .models import Paciente

class PostAdmin(admin.ModelAdmin):
	list_display = ('id_unico', 'nombre', 'apellido', 'creado_en', 'actualizado_en')

#admin.site.site_header = 'Clinic docs'
#admin.site.site_title = 'Clinic docs'
admin.site.register(Paciente, PostAdmin)