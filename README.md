<div align="center">
  <img src="logo.png" alt="clinic-docs logo" height="80">
</div>

## Project setup
```
pip install -r requirements.txt
```

### Compiles and hot-reloads for development
```
python manage.py runserver
```

## Creators

**Emmanuel Rodriguez Hernandez** 
- [Gitlab](https://gitlab.com/)
- [Twitter](https://twitter.com/)

**Alvaro Azael Rodriguez Rodriguez** 
- [Gitlab](https://gitlab.com/azael_rguez)
- [Twitter](https://twitter.com/azael_rguez)

## Copyright and license

This project is licensed under the [MIT License](LICENSE).<br>
Docs released under Creative Commons [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).